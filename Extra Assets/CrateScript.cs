﻿using UnityEngine;
using System.Collections;

public class CrateScript : MonoBehaviour {

    static GameObject[] testAgents;
    static GameObject[] cops;
    public static GameObject[] spots;
    public GameObject crate;
    public static GameObject newCrate;

    static ArrayList waitingList;
    static ArrayList waitingList2;

    private static bool isRunning = false;
    private static bool destroyed = false;
    private static bool leaving = false;

    private static int increment = 0;

    // Use this for initialization
    void Start () {
        newCrate = (GameObject)Instantiate(crate, new Vector3(4, 20, -2.5f), Quaternion.identity);
        newCrate.AddComponent<Rigidbody>().useGravity = true;
        newCrate.AddComponent<BoxCollider>();

        GameObject[] temp = GameObject.FindGameObjectsWithTag("Test Agent");
        testAgents = new GameObject[4];
        for(int i = 0; i < 4; i++)
        {
            testAgents[i] = temp[i + 1];
        }
        spots = GameObject.FindGameObjectsWithTag("Spot");
        cops = GameObject.FindGameObjectsWithTag("Cop");

        waitingList = new ArrayList();
        for (int i = 0; i < testAgents.Length; i++)
        {
            waitingList.Add(false);
        }
        waitingList2 = new ArrayList();
        for (int i = 0; i < cops.Length; i++)
        {
            waitingList2.Add(false);
        }
    }
	
	// Update is called once per frame
	void Update () {
        checkWaiting();

        if (isRunning)
            setSpeed();

	    if(!destroyed && newCrate.transform.position.y < 3)
        {
            for(int i = 0; i < testAgents.Length; i++)
            {
                SmartObject so = testAgents[i].GetComponent<SmartObject>();
                SmartCharacter[] chars = { testAgents[i].GetComponent<SmartCharacter>() };
                if (!(so.Behavior.Status == BehaviorStatus.InEvent) && !(bool)waitingList[i])
                {
                    BehaviorEvent e = new BehaviorEvent(doEvent => chars[0].Node_CratePanic(), chars);
                    e.StartEvent(1f);
                    waitingList.RemoveAt(i);
                    waitingList.Insert(i, true);
                }
            }
        }

        if (isRunning && !leaving)
        {
            for (int i = 0; i < cops.Length; i++)
            {
                SmartObject so = cops[i].GetComponent<SmartObject>();
                SmartCharacter[] chars = { cops[i].GetComponent<SmartCharacter>() };
                if (!(so.Behavior.Status == BehaviorStatus.InEvent) && !(bool)waitingList2[i])
                {
                    BehaviorEvent e = new BehaviorEvent(doEvent => chars[0].Node_Investigate(), chars);
                    e.StartEvent(1f);
                    waitingList2.RemoveAt(i);
                    waitingList2.Insert(i, true);
                }
            }
        }

        if (leaving)
        {
            for (int i = 0; i < cops.Length; i++)
            {
                SmartObject so = cops[i].GetComponent<SmartObject>();
                SmartCharacter[] chars = { cops[i].GetComponent<SmartCharacter>() };
                int k = UnityEngine.Random.Range(0, 4);
                Vector3 t = spots[k].transform.position;
                if (!(so.Behavior.Status == BehaviorStatus.InEvent) && !(bool)waitingList2[i])
                {
                    BehaviorEvent e = new BehaviorEvent(doEvent => chars[0].Node_CrowdPump2(), chars);
                    e.StartEvent(3f);
                    waitingList2.RemoveAt(i);
                    waitingList2.Insert(i, true);
                }
            }
        }
    }

    public static bool endScene()
    {
        increment++;
        if(increment == 4)
            Application.LoadLevel("Final Scene");
        return true;
    }

    public static bool canContinue()
    {
        increment++;
        if (increment != 4)
            return false;
        increment = 0;
        leaving = true;
        destroyCrate();
        for (int i = 0; i < waitingList2.Count; i++)
        {
            waitingList2.RemoveAt(i);
            waitingList2.Insert(i, false);
        }
        return true;
    }

    public static bool destroyCrate()
    {
        Destroy(newCrate);
        destroyed = true;
        return true;
    }

    void setSpeed()
    {
        for(int i = 0; i < testAgents.Length; i++)
        {
            testAgents[i].GetComponent<Animator>().SetFloat("Speed", 10f);
        }
    }

    public static bool initializeSpeed()
    {
        isRunning = true;
        return true;
    }

    static void checkWaiting()
    {
        for (int i = 0; i < waitingList.Count; i++)
        {
            if ((bool)waitingList[i] == true && testAgents[i].GetComponent<SmartObject>().Behavior.Status == BehaviorStatus.InEvent)
            {
                waitingList.RemoveAt(i);
                waitingList.Insert(i, false);
            }
        }
    }
}
