﻿using UnityEngine;
using System.Collections;

public class CanvasText: MonoBehaviour {

    public UnityEngine.UI.Text selected1;
    public UnityEngine.UI.Text selected2;
    public UnityEngine.UI.Text selected3;
    public UnityEngine.UI.Text selected4;

    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        int i;
        for (i = 0; i < MainController2.selected.Count; i++)
        {
            int index = (int)MainController2.selected[i];
            if(i == 0)
                selected1.text = MainController2.testAgents[index].name;
            if(i == 1)
                selected2.text = MainController2.testAgents[index].name;
            if (i == 2)
                selected3.text = MainController2.testAgents[index].name;
            if (i == 3)
                selected4.text = MainController2.testAgents[index].name;
        }
        if (i == 0)
        {
            selected1.text = "";
            selected2.text = "";
            selected3.text = "";
            selected4.text = "";
        }
        if (i == 1)
        {
            selected2.text = "";
            selected3.text = "";
            selected4.text = "";
        }
        if (i == 2)
        {
            selected3.text = "";
            selected4.text = "";
        }
        if (i == 3)
        {
            selected4.text = "";
        }
    }
}
