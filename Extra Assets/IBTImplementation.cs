﻿using UnityEngine;
using System.Collections;
using TreeSharpPlus;

public class IBTImplementation : MonoBehaviour {


    GameObject[] testAgents;
    GameObject[] spots;
    GameObject[] fightSpots;

    int selectedSpot;
    int[] fighters;
    bool[] able;

    private int target = 0;

    public static string arcID = "fightArc";
    static bool done = false;
    static bool fighting = false;
    static bool waving = false;
    static bool crowdPumping = false;
    static bool contemplating = false;
    private static ArrayList fightingList;
    private static ArrayList wavingList;
    private static ArrayList crowdPumpingList;
    private static ArrayList contemplatingList;
    private static ArrayList behaviorEvents;

    private static Queue fightingQueue;
    private static Queue wavingQueue;
    private static Queue crowdPumpingQueue;
    private static Queue contemplatingQueue;

    // Use this for initialization
    void Start () {
        testAgents = GameObject.FindGameObjectsWithTag("Test Agent");
        Debug.Log("Initializing Main Controller ... ");
	    spots = GameObject.FindGameObjectsWithTag("Spot");
        if (spots.Length > 0) Debug.Log("Found " + spots.Length + " spots for wandering ...");
        fightSpots = GameObject.FindGameObjectsWithTag("FightSpot");
        if (fightSpots.Length > 0) Debug.Log("Found " + fightSpots.Length + " spots for fighting ...");
        selectedSpot = UnityEngine.Random.Range(0, fightSpots.Length);

        fightingQueue = new Queue();
        wavingQueue = new Queue();
        crowdPumpingQueue = new Queue();
        contemplatingQueue = new Queue();

        fightingList = new ArrayList();
        wavingList = new ArrayList();
        crowdPumpingList = new ArrayList();
        contemplatingList = new ArrayList();
        behaviorEvents = new ArrayList();
        for (int i = 0; i < testAgents.Length; i++)
        {
            fightingList.Add(false);
            wavingList.Add(false);
            crowdPumpingList.Add(false);
            contemplatingList.Add(false);
            behaviorEvents.Add(null);
        }

        fighters = new int[2];
        if (testAgents.Length >= 2)
            while (fighters[0] == fighters[1])
            {
                fighters[0] = UnityEngine.Random.Range(0, testAgents.Length);
                fighters[1] = UnityEngine.Random.Range(0, testAgents.Length);
            }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            GameObject g = GetClickTarget();
            if(g.tag.Equals("Test Agent"))
            {
                int i;
                for(i = 0; i < testAgents.Length; i++)
                {
                    if (testAgents[i] == g)
                        break;
                }
                fighters[target] = i;
                if (target == 0)
                    target = 1;
                else
                    target = 0;
            }
        }

        if (Input.GetKeyDown("0"))
        {
            Application.LoadLevel("Final Scene");
        }

        if (Input.GetKeyUp("9"))
        {
            for(int i = 0; i < behaviorEvents.Count; i++)
            {
                if (behaviorEvents[i] != null)
                {
                    ((BehaviorEvent)behaviorEvents[i]).StopEvent();
                    setFighting();
                }
            }
        }

        if (Input.GetKeyDown("1") && fighting == false)
        {
            fightingList.RemoveAt(fighters[0]);
            fightingList.Insert(fighters[0], true);
            fightingList.RemoveAt(fighters[1]);
            fightingList.Insert(fighters[1], true);
            fighting = true;
        }
        if (Input.GetKeyDown("2") && waving == false)
        {
            wavingList.RemoveAt(fighters[0]);
            wavingList.Insert(fighters[0], true);
            wavingList.RemoveAt(fighters[1]);
            wavingList.Insert(fighters[1], true);

            waving = false;
        }
        if (Input.GetKeyDown("3") && crowdPumping == false)
        {
            if (target == 1)
            {
                crowdPumpingList.RemoveAt(fighters[0]);
                crowdPumpingList.Insert(fighters[0], true);
            }
            else
            {
                crowdPumpingList.RemoveAt(fighters[1]);
                crowdPumpingList.Insert(fighters[1], true);
            }

            crowdPumping = false;
        }
        if (Input.GetKeyDown("4") && contemplating == false)
        {
            if (target == 1)
            {
                contemplatingList.RemoveAt(fighters[0]);
                contemplatingList.Insert(fighters[0], true);
            }
            else
            {
                contemplatingList.RemoveAt(fighters[1]);
                contemplatingList.Insert(fighters[1], true);
            }

            contemplating = false;
        }

        if (Input.GetKeyDown("space"))
        {
            done = true;
        }

        
        SmartObject so = testAgents[fighters[0]].GetComponent<SmartObject>();
        SmartObject other;
            other = testAgents[fighters[1]].GetComponent<SmartObject>();

        so.Set(StateName.RoleFighter);
        other.Set(StateName.RoleFighter);
        Vector3 v = Vector3.zero;
        if (!(so.Behavior.Status == BehaviorStatus.InEvent) && !(other.Behavior.Status == BehaviorStatus.InEvent) && (bool)fightingList[fighters[0]] && (bool)fightingList[fighters[1]])
        {
            SmartCharacter[] chars = { so.GetComponent<SmartCharacter>(), other.GetComponent<SmartCharacter>() };

            bool found = false;
            while (!found)
            {
                int index = UnityEngine.Random.Range(0, fightSpots.Length);
                Transform t = fightSpots[index].transform;
                if (Vector3.Distance(t.position, so.transform.position) > 7f)
                {
                    v = t.position;
                    found = true;
                }
            }
            v.y = 0;
            Val<Vector3> v1 = Val.V(() => v);
            Val<float> v2 = Val.V(() => 0.8f);
            BehaviorEvent e = new BehaviorEvent(doEvent => chars[0].Node_FightArc(other.GetComponent<SmartCharacter>(), v1, v2), chars);
            e.StartEvent(1.0f);
            behaviorEvents.RemoveAt(fighters[0]);
            behaviorEvents.Insert(fighters[0], e);
            behaviorEvents.RemoveAt(fighters[1]);
            behaviorEvents.Insert(fighters[1], e);
            fightingQueue.Enqueue(fighters);
        }
        
        if (!(so.Behavior.Status == BehaviorStatus.InEvent) && !(other.Behavior.Status == BehaviorStatus.InEvent) && (bool)wavingList[fighters[0]] && (bool)wavingList[fighters[1]])
        {
            SmartCharacter[] chars = { so.GetComponent<SmartCharacter>()};
            SmartCharacter[] chars2 = { other.GetComponent<SmartCharacter>() };

            BehaviorEvent e = new BehaviorEvent(doEvent => chars[0].Node_WaveTo(other.GetComponent<SmartCharacter>()), chars);
            BehaviorEvent e2 = new BehaviorEvent(doEvent => chars2[0].Node_WaveTo(so.GetComponent<SmartCharacter>()), chars2);
            e.StartEvent(1.0f);
            e2.StartEvent(1.0f);
            wavingQueue.Enqueue(fighters);
        }

        for (int i = 0; i < testAgents.Length; i++)
        {
            so = testAgents[i].GetComponent<SmartObject>();
            if (!(so.Behavior.Status == BehaviorStatus.InEvent) && (bool)crowdPumpingList[i])
            {
                SmartCharacter[] chars = { so.GetComponent<SmartCharacter>() };
                BehaviorEvent e = new BehaviorEvent(doEvent => chars[0].Node_CrowdPump(), chars);
                e.StartEvent(1.0f);
                crowdPumpingQueue.Enqueue(fighters);
                return;
            }

            if (!(so.Behavior.Status == BehaviorStatus.InEvent) && (bool)contemplatingList[i])
            {
                SmartCharacter[] chars = { so.GetComponent<SmartCharacter>() };
                BehaviorEvent e = new BehaviorEvent(doEvent => chars[0].Node_Contemplate(), chars);
                e.StartEvent(1.0f);
                contemplatingQueue.Enqueue(fighters);
                return;
            }

            if (!(so.Behavior.Status == BehaviorStatus.InEvent) && !(bool)fightingList[i] && !(bool)wavingList[i])
            {
                SmartCharacter[] chars = { so.GetComponent<SmartCharacter>() };
                bool found = false;
                while (!found)
                {
                    int index = UnityEngine.Random.Range(0, spots.Length);
                    Transform t = spots[index].transform;
                    if (Vector3.Distance(t.position, so.transform.position) > 4f)
                    {
                        v = t.position;
                        found = true;
                    }
                }
                v.y = 0;
                Val<Vector3> v1 = Val.V(() => v);
                Val<float> v2 = Val.V(() => 0.8f);
                BehaviorEvent e = new BehaviorEvent(doEvent => chars[0].Node_GoToUpToRadius(v1, v2), chars);
                e.StartEvent(1.0f);
                return;
                }
            }
    }

    public static bool isDone(){
        if (!done)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static bool setFighting()
    {
        int[] characters = (int[])fightingQueue.Dequeue();
        fightingList.RemoveAt(characters[0]);
        fightingList.Insert(characters[0], false);
        fightingList.RemoveAt(characters[1]);
        fightingList.Insert(characters[1], false);
        fighting = false;
        return true;
    }

    public static bool setWaving()
    {
        int[] characters = (int[])wavingQueue.Dequeue();
        wavingList.RemoveAt(characters[0]);
        wavingList.Insert(characters[0], false);
        wavingList.RemoveAt(characters[1]);
        wavingList.Insert(characters[1], false);
        waving = false;
        return true;
    }

    public static bool setCrowdPumping()
    {
        int[] characters = (int[])crowdPumpingQueue.Dequeue();
        crowdPumpingList.RemoveAt(characters[0]);
        crowdPumpingList.Insert(characters[0], false);
        crowdPumpingList.RemoveAt(characters[1]);
        crowdPumpingList.Insert(characters[1], false);
        crowdPumping = false;
        return true;
    }

    public static bool setContemplating()
    {
        int[] characters = (int[])contemplatingQueue.Dequeue();
        contemplatingList.RemoveAt(characters[0]);
        contemplatingList.Insert(characters[0], false);
        contemplatingList.RemoveAt(characters[1]);
        contemplatingList.Insert(characters[1], false);
        contemplating = false;
        return true;
    }

    public static bool isFightArc()
    {
        if (arcID.Equals("fightArc"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    GameObject GetClickTarget()
    {
        RaycastHit hitInfo = new RaycastHit();
        bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);
        if (hit)
        {
            if (hitInfo.rigidbody != null)
                Debug.Log("Hit " + hitInfo.rigidbody.gameObject.name);
        }
        return hitInfo.transform.gameObject;

    }
}
