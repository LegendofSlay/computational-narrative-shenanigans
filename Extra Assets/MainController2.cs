﻿using UnityEngine;
using System.Collections;
using TreeSharpPlus;

public class MainController2 : MonoBehaviour {


    public static GameObject[] testAgents;
    GameObject[] spots;
    GameObject[] fightSpots;

    int selectedSpot;
    public static int[] fighters;
    bool[] able;

    public static ArrayList selected;
    private float priority = 1f;

    public static string arcID = "fightArc";
    static ArrayList waitingList;

    // Use this for initialization
    void Start () {
        GameObject[] temp = GameObject.FindGameObjectsWithTag("Test Agent");
        testAgents = new GameObject[4];
        for (int i = 0; i < 4; i++)
        {
            testAgents[i] = temp[i + 1];
        }
        Debug.Log("Initializing Main Controller ... ");
	    spots = GameObject.FindGameObjectsWithTag("Spot");
        if (spots.Length > 0) Debug.Log("Found " + spots.Length + " spots for wandering ...");
        fightSpots = GameObject.FindGameObjectsWithTag("FightSpot");
        if (fightSpots.Length > 0) Debug.Log("Found " + fightSpots.Length + " spots for fighting ...");
        selectedSpot = UnityEngine.Random.Range(0, fightSpots.Length);

        selected = new ArrayList();
        waitingList = new ArrayList();
        for(int i = 0; i < testAgents.Length; i++)
        {
            waitingList.Add(false);
        }

        fighters = new int[2];
        if (testAgents.Length >= 2)
            while (fighters[0] == fighters[1])
            {
                fighters[0] = UnityEngine.Random.Range(0, testAgents.Length);
                fighters[1] = UnityEngine.Random.Range(0, testAgents.Length);
            }
    }

    // Update is called once per frame
    void Update()
    {
        checkWaiting();

        if (Input.GetMouseButtonDown(0))
        {
            GameObject g = GetClickTarget();
            if(g.tag.Equals("Test Agent"))
            {
                int i;
                for(i = 0; i < selected.Count; i++)
                {
                    if (testAgents[(int)selected[i]] == g)
                    {
                        selected.RemoveAt(i);
                        return;
                    }
                }
                for(i = 0; i < testAgents.Length; i++)
                {
                    if (testAgents[i] == g)
                        break;
                }
                selected.Add(i);
            }
            else
            {
                selected.Clear();
            }
        }

        if (Input.GetKeyDown("0"))
        {
            Application.LoadLevel("Final Scene");
        }

        if (Input.GetKeyUp("8"))
        {
            SmartObject obj = testAgents[fighters[0]].GetComponent<SmartObject>();
            Debug.Log(obj.Behavior.Status);
        }

        if (Input.GetKeyDown("1"))
        {
            int num = selected.Count / 2;
            for (int i = 0; i < num; i++)
            {
                int[] chars = { (int)selected[i*2], (int)selected[i*2 + 1] };
                runFighting(chars);
            }
        }

        if (Input.GetKeyDown("2"))
        {
            int num = selected.Count / 2;
            for (int i = 0; i < num; i++)
            {
                int[] chars = { (int)selected[i * 2], (int)selected[i * 2 + 1] };
                runWaving(chars);
            }
        }

        if (Input.GetKeyDown("3"))
        {
            for (int i = 0; i < selected.Count; i++)
            {
                int[] chars = { (int)selected[i]};
                runCrowdPumping(chars);
            }
        }

        if (Input.GetKeyDown("4"))
        {
            for (int i = 0; i < selected.Count; i++)
            {
                int[] chars = { (int)selected[i]};
                runContemplating(chars);
            }
        }

        for (int i = 0; i < testAgents.Length; i++)
        {
            SmartObject so = testAgents[i].GetComponent<SmartObject>();

            if (!(so.Behavior.Status == BehaviorStatus.InEvent) && !(bool)waitingList[i])
            {
                runIdle(so.gameObject.GetComponent<SmartCharacter>());
                return;
            }
        }
    }

    void runIdle(SmartCharacter character)
    {
        Vector3 v = Vector3.zero;
        SmartCharacter[] chars = { character };
        bool found = false;
        while (!found)
        {
            int index = UnityEngine.Random.Range(0, spots.Length);
            Transform t = spots[index].transform;
            if (Vector3.Distance(t.position, character.transform.position) > 4f)
            {
                v = t.position;
                found = true;
            }
        }
        v.y = 0;
        Val<Vector3> v1 = Val.V(() => v);
        Val<float> v2 = Val.V(() => 0.8f);
        BehaviorEvent e = new BehaviorEvent(doEvent => chars[0].Node_GoToUpToRadius(v1, v2), chars);
        e.StartEvent(priority);
    }

    void runFighting(int[] charNums)
    {
        SmartCharacter[] chars = { testAgents[charNums[0]].GetComponent<SmartCharacter>(), testAgents[charNums[1]].GetComponent<SmartCharacter>() };
        SmartObject[] obj = { chars[0].gameObject.GetComponent<SmartObject>(), chars[1].gameObject.GetComponent<SmartObject>() };
        Vector3 v = Vector3.zero;
        bool found = false;
        while (!found)
        {
            int index = UnityEngine.Random.Range(0, fightSpots.Length);
            Transform t = fightSpots[index].transform;
            v = t.position;
            found = true;
        }
        Val<Vector3> v1 = Val.V(() => v);
        Val<float> v2 = Val.V(() => 0.8f);

        BehaviorEvent oldEvent1 = obj[0].Behavior.CurrentEvent, oldEvent2 = obj[1].Behavior.CurrentEvent;
        if (oldEvent1 != null)
        {
            oldEvent1.StopEvent();
            obj[0].Behavior.FinishEvent();
        }
        if (oldEvent2 != null)
        {
            oldEvent2.StopEvent();
            obj[1].Behavior.FinishEvent();
        }

        BehaviorEvent e = new BehaviorEvent(doEvent => chars[0].Node_FightArc(chars[1], v1, v2), chars);
        priority++;
        e.StartEvent(priority);

        waitingList.RemoveAt(charNums[0]);
        waitingList.Insert(charNums[0], true);
        waitingList.RemoveAt(charNums[1]);
        waitingList.Insert(charNums[1], true);
    }

    void runWaving(int[] charNums)
    {
        SmartCharacter[] chars = { testAgents[charNums[0]].GetComponent<SmartCharacter>(), testAgents[charNums[1]].GetComponent<SmartCharacter>() };
        SmartObject[] obj = { chars[0].gameObject.GetComponent<SmartObject>(), chars[1].gameObject.GetComponent<SmartObject>() };

        BehaviorEvent oldEvent1 = obj[0].Behavior.CurrentEvent, oldEvent2 = obj[1].Behavior.CurrentEvent;
        if (oldEvent1 != null)
        {
            oldEvent1.StopEvent();
            obj[0].Behavior.FinishEvent();
        }
        if (oldEvent2 != null)
        {
            oldEvent2.StopEvent();
            obj[1].Behavior.FinishEvent();
        }

        SmartCharacter[] charsTemp1 = { chars[0] };
        SmartCharacter[] charsTemp2 = { chars[1] };
        BehaviorEvent e = new BehaviorEvent(doEvent => chars[0].Node_WaveTo(chars[1]), charsTemp1);
        BehaviorEvent e2 = new BehaviorEvent(doEvent => chars[1].Node_WaveTo(chars[0]), charsTemp2);
        priority++;
        e.StartEvent(priority);
        e2.StartEvent(priority);

        waitingList.RemoveAt(charNums[0]);
        waitingList.Insert(charNums[0], true);
        waitingList.RemoveAt(charNums[1]);
        waitingList.Insert(charNums[1], true);
    }

    void runCrowdPumping(int[] charNums)
    {
        SmartCharacter[] chars = { testAgents[charNums[0]].GetComponent<SmartCharacter>() };
        SmartObject[] obj = { chars[0].gameObject.GetComponent<SmartObject>()};

        BehaviorEvent oldEvent1 = obj[0].Behavior.CurrentEvent;
        if (oldEvent1 != null)
        {
            oldEvent1.StopEvent();
            obj[0].Behavior.FinishEvent();
        }

        BehaviorEvent e = new BehaviorEvent(doEvent => chars[0].Node_CrowdPump(), chars);
        priority++;
        e.StartEvent(priority);

        waitingList.RemoveAt(charNums[0]);
        waitingList.Insert(charNums[0], true);
    }

    void runContemplating(int[] charNums)
    {
        SmartCharacter[] chars = { testAgents[charNums[0]].GetComponent<SmartCharacter>() };
        SmartObject[] obj = { chars[0].gameObject.GetComponent<SmartObject>() };

        BehaviorEvent oldEvent1 = obj[0].Behavior.CurrentEvent;
        if (oldEvent1 != null)
        {
            oldEvent1.StopEvent();
            obj[0].Behavior.FinishEvent();
        }

        BehaviorEvent e = new BehaviorEvent(doEvent => chars[0].Node_Contemplate(), chars);
        priority++;
        e.StartEvent(priority);

        waitingList.RemoveAt(charNums[0]);
        waitingList.Insert(charNums[0], true);
    }

    static void checkWaiting()
    {
        for(int i = 0; i < waitingList.Count; i++)
        {
            if((bool)waitingList[i] == true && testAgents[i].GetComponent<SmartObject>().Behavior.Status == BehaviorStatus.InEvent)
            {
                waitingList.RemoveAt(i);
                waitingList.Insert(i, false);
            }
        }
    }

    public static bool isFightArc()
    {
        if (arcID.Equals("fightArc"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    GameObject GetClickTarget()
    {
        RaycastHit hitInfo = new RaycastHit();
        bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);
        if (hit)
        {
            if (hitInfo.rigidbody != null)
                Debug.Log("Hit " + hitInfo.rigidbody.gameObject.name);
        }
        return hitInfo.transform.gameObject;

    }
}
