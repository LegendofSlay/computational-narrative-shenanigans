﻿using UnityEngine;
using System.Collections;

public class UserInputController : MonoBehaviour {

	bool inSelectionMode;

	// Use this for initialization
	void Start () {
		this.inSelectionMode = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown (0)) {
			Debug.Log ("Pressed left click.");
			GameObject g = GetClickTarget ();
			Animator animator = g.GetComponent<Animator>();
			//animator.SetTrigger ("Breakdance");
			//animator.SetTrigger(Animator.StringToHash("Navigation.Body"));
			animator.SetTrigger (Animator.StringToHash("Jump");
			//if (!this.inSelectionMode) this.inSelectionMode = true;

		}
	}

	GameObject GetClickTarget() {
		RaycastHit hitInfo = new RaycastHit();
		bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);
		if (hit) {
			Debug.Log ("Hit " + hitInfo.transform.gameObject.name);
		}
		return hitInfo.transform.gameObject;

	}




}
