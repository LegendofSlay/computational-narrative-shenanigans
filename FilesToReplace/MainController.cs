﻿using UnityEngine;
using System.Collections;
using TreeSharpPlus;

public class MainController : MonoBehaviour {


    GameObject[] testAgents;
    GameObject[] spots;
    GameObject[] fightSpots;

    int selectedSpot;
    int[] fighters;

    private bool done = false;
    private bool fighting = false;

    // Use this for initialization
    void Start () {
        testAgents = GameObject.FindGameObjectsWithTag("Test Agent");
        Debug.Log("Initializing Main Controller ... ");
	    spots = GameObject.FindGameObjectsWithTag("Spot");
        if (spots.Length > 0) Debug.Log("Found " + spots.Length + " spots for wandering ...");
        fightSpots = GameObject.FindGameObjectsWithTag("FightSpot");
        if (fightSpots.Length > 0) Debug.Log("Found " + fightSpots.Length + " spots for fighting ...");
        selectedSpot = UnityEngine.Random.Range(0, fightSpots.Length);

        fighters = new int[2];
        while (fighters[0] == fighters[1])
        {
            fighters[0] = UnityEngine.Random.Range(0, testAgents.Length);
            fighters[1] = UnityEngine.Random.Range(0, testAgents.Length);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("0"))
        {
            Application.LoadLevel("Town Scene");
        }

        if (Input.GetKeyDown("space") && fighting == true)
        {
            for (int i = 0; i < fighters.Length; i++)
            {
                SmartObject so = testAgents[fighters[i]].GetComponent<SmartObject>();
                so.Set(~StateName.isReadyToFight);
            }
        }

        for (int i = 0; i < fighters.Length; i++)
        {
            SmartObject so = testAgents[fighters[i]].GetComponent<SmartObject>();
            SmartObject other;
            if(i == 0)
                other = testAgents[fighters[1]].GetComponent<SmartObject>();
            else
                other = testAgents[fighters[0]].GetComponent<SmartObject>();

            Vector3 v = Vector3.zero;
            if (!(so.Behavior.Status == BehaviorStatus.InEvent))
            {
                SmartCharacter[] chars = { so.GetComponent<SmartCharacter>() };
                if(!done)
                    so.Set(StateName.RoleFighter);
                if (done && so.Require(~StateName.RoleFighter) && so.Require(~StateName.isReadyToFight) && so.Require(~StateName.isFighting))
                {
                    bool found = false;
                    while (!found)
                    {
                        int index = UnityEngine.Random.Range(0, spots.Length);
                        Transform t = spots[index].transform;
                        if (Vector3.Distance(t.position, so.transform.position) > 7f)
                        {
                            v = t.position;
                            found = true;
                        }
                    }
                    v.y = 0;
                    Val<Vector3> v1 = Val.V(() => v);
                    Val<float> v2 = Val.V(() => 0.8f);
                    BehaviorEvent e = new BehaviorEvent(doEvent => chars[0].Node_GoToUpToRadius(v1, v2), chars);
                    e.StartEvent(1.0f);
                }

                if (!done && so.Require(StateName.RoleFighter) && so.Require(~StateName.isReadyToFight) && so.Require(~StateName.isFighting))
                {
                    Transform t = fightSpots[selectedSpot].transform;
                    v = t.position;
                    v.y = 0;
                    Val<Vector3> v1 = Val.V(() => v);
                    Val<float> v2 = Val.V(() => 0.8f);
                    BehaviorEvent e = new BehaviorEvent(doEvent => chars[0].Node_FighterGoUpToRadius(v1, v2), chars);
                    e.StartEvent(1.0f);
                    if (i == 1)
                        done = true;
                    continue;
                }
                if (so.Require(StateName.RoleFighter) && so.Require(StateName.isReadyToFight) && so.Require(~StateName.isFighting))
                {
                    BehaviorEvent e2 = new BehaviorEvent(doEvent => chars[0].Node_Disrespect((SmartCharacter)other), chars);
                    e2.StartEvent(1.0f);
                    continue;
                }
                if (so.Require(StateName.RoleFighter) && so.Require(StateName.isReadyToFight) && so.Require(StateName.isFighting))
                {
                    BehaviorEvent e3 = new BehaviorEvent(doEvent => chars[0].Node_Fight((SmartCharacter)other), chars);
                    e3.StartEvent(1.0f);
                    if(i == 1)
                        fighting = true;
                    continue;
                }
                if (so.Require(StateName.RoleFighter) && so.Require(~StateName.isReadyToFight) && so.Require(StateName.isFighting))
                {
                    BehaviorEvent e4 = new BehaviorEvent(doEvent => chars[0].Node_MakeUp((SmartCharacter)other), chars);
                    e4.StartEvent(1.0f);
                    fighting = false;
                    continue;
                }
            }
        }

        // Code for the roaming civilians
        for(int i = 0; i < testAgents.Length; i++) {

            if (i == fighters[0] || i == fighters[1])
                continue;

            SmartObject so = testAgents[i].GetComponent<SmartObject>();
            Vector3 v = Vector3.zero;
            if (!(so.Behavior.Status == BehaviorStatus.InEvent)) {
                bool found = false;
                while (!found) {
                    int index = UnityEngine.Random.Range(0, spots.Length);
                    Transform t = spots[index].transform;
                    if (Vector3.Distance(t.position, so.transform.position) > 7f) {
                        v = t.position;
                        found = true;
                    }
                }
                v.y = 0;
                Val<Vector3> v1 = Val.V(() => v);
                Val<float> v2 = Val.V(() => 0.8f);
                SmartCharacter[] chars = { so.GetComponent<SmartCharacter>() };
                BehaviorEvent e = new BehaviorEvent(doEvent => chars[0].Node_GoToUpToRadius(v1, v2), chars);
                e.StartEvent(1.0f);
            }
        }
    }
}
